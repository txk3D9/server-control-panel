<?php
ini_set('display_errors', 1);
error_reporting (E_ERROR);

require_once dirname(__FILE__).'/vendor/autoload.php';

$name = isset($argv[1]) ? $argv[1] : $_GET['name'];
if(!$name) {
	throw new \Exception('No cronjob set');
}
$cronjobHandler = new \ServerControlPanel\Handlers\CronjobHandler();
$cronjobHandler->handleRequest($name);
