<?php

namespace ServerControlPanel\Models;

use ServerControlPanel\Models\Information;
use ServerControlPanel\Traits\OnlineableTrait;
use ServerControlPanel\Traits\StartStoppable;

class Server extends HandleableEntity {

	/**
	 * @var string
	 */
	protected $username;
	/**
	 * @var string
	 */
	protected $password;
	/**
	 * @var string
	 */
	protected $localIp;
	/**
	 * @var string
	 */
	protected $macAddress;
	/**
	 * @var Application[]
	 */
	protected $applications = [];
	/**
	 * @var Information[]
	 */
	protected $informations = [];
	/**
	 * @var integer
	 */
	protected $autoShutdownMaxUpTime;
	/**
	 * @var bool
	 */
	protected $autoShutdownEnabled;
	/**
	 * @var integer
	 */
	protected $nextShutdownAt;

	/**
	 * @return string
	 */
	public function getUsername(): string {
		return $this->username;
	}

	/**
	 * @param string $username
	 */
	public function setUsername(string $username): void {
		$this->username = $username;
	}

	/**
	 * @return string
	 */
	public function getPassword(): string {
		return $this->password;
	}

	/**
	 * @param string $password
	 */
	public function setPassword(string $password): void {
		$this->password = $password;
	}

	/**
	 * @return string
	 */
	public function getLocalIp(): string {
		return $this->localIp;
	}

	/**
	 * @param string $localIp
	 */
	public function setLocalIp(string $localIp): void {
		$this->localIp = $localIp;
	}

	/**
	 * @return string
	 */
	public function getMacAddress(): string {
		return $this->macAddress;
	}

	/**
	 * @param string $macAddress
	 */
	public function setMacAddress(string $macAddress): void {
		$this->macAddress = $macAddress;
	}

	/**
	 * @return Application[]
	 */
	public function getApplications(): array {
		return $this->applications;
	}

	/**
	 * @param Application[] $applications
	 */
	public function setApplications(array $applications): void {
		$this->applications = $applications;
	}

	/**
	 * @return Information[]
	 */
	public function getInformations(): array {
		return $this->informations;
	}

	/**
	 * @param Information[] $informations
	 */
	public function setInformations(array $informations): void {
		$this->informations = $informations;
	}

	/**
	 * @return bool
	 */
	public function getStartable(): bool {
		return $this->startable;
	}

	/**
	 * @param bool $startable
	 */
	public function setStartable(bool $startable): void {
		$this->startable = $startable;
	}

	/**
	 * @return bool
	 */
	public function getStoppable(): bool {
		return $this->stoppable;
	}

	/**
	 * @param bool $stoppable
	 */
	public function setStoppable(bool $stoppable): void {
		$this->stoppable = $stoppable;
	}

	/**
	 * @return int
	 */
	public function getAutoShutdownMaxUpTime(): int {
		return $this->autoShutdownMaxUpTime;
	}

	/**
	 * @param int $autoShutdownMaxUpTime
	 */
	public function setAutoShutdownMaxUpTime(int $autoShutdownMaxUpTime): void {
		$this->autoShutdownMaxUpTime = $autoShutdownMaxUpTime;
	}

	/**
	 * @return bool
	 */
	public function getAutoShutdownEnabled(): bool {
		return $this->autoShutdownEnabled;
	}

	/**
	 * @param bool $autoShutdownEnabled
	 */
	public function setAutoShutdownEnabled(bool $autoShutdownEnabled): void {
		$this->autoShutdownEnabled = $autoShutdownEnabled;
	}

	/**
	 * @return int
	 */
	public function getNextShutdownAt(): int {
		return $this->nextShutdownAt;
	}

	/**
	 * @param int $nextShutdownAt
	 */
	public function setNextShutdownAt(int $nextShutdownAt): void {
		$this->nextShutdownAt = $nextShutdownAt;
	}
}