<?php

namespace ServerControlPanel\Models;

use ServerControlPanel\Models\Application;

class Config {

	/**
	 * @var Server[]
	 */
	protected $servers = [];

	/**
	 * @return Server[]
	 */
	public function getServers(): array {
		return $this->servers;
	}

	/**
	 * @param Server[] $servers
	 */
	public function setServers(array $servers): void {
		$this->servers = $servers;
	}
}