<?php

namespace ServerControlPanel\Models\Configuration;

class ConfigFile {

	public function __construct($relativePath, $readOnly = false) {
		$this->relativePath = $relativePath;
		$this->readOnly = $readOnly;
	}

	/**
	 * @var string
	 */
	protected $relativePath;

	/**
	 * @var  booleam
	 */
	protected $readOnly = false;

	/**
	 * @var  string
	 */
	protected $content;

	public function getFileName() {
		return end(explode("/", $this->relativePath));
	}

	/**
	 * @return string
	 */
	public function getRelativePath(): string {
		return $this->relativePath;
	}

	/**
	 * @param string $relativePath
	 */
	public function setRelativePath(string $relativePath): void {
		$this->relativePath = $relativePath;
	}

	/**
	 * @return string
	 */
	public function getContent(): string {
		return $this->content;
	}

	/**
	 * @param string $content
	 */
	public function setContent(string $content): void {
		$this->content = $content;
	}

	/**
	 * @return booleam
	 */
	public function getReadOnly() {
		return $this->readOnly;
	}

	/**
	 * @param booleam $readOnly
	 */
	public function setReadOnly($readOnly): void {
		$this->readOnly = $readOnly;
	}
}