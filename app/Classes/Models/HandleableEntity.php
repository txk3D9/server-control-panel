<?php

namespace ServerControlPanel\Models;

use ServerControlPanel\Traits\StartStoppable;

abstract class HandleableEntity {

	/**
	 * @var string
	 */
	protected $name;
	/**
	 * @var string
	 */
	protected $identifier;
	/**
	 * @var bool
	 */
	protected $startable = false;
	/**
	 * @var bool
	 */
	protected $stoppable = false;
	/**
	 * @var bool
	 */
	protected $isOnline = false;
	/**
	 * @var bool
	 */
	protected $updatable = false;
	/**
	 * @var bool
	 */
	protected $backupable = false;

	/**
	 * @return bool
	 */
	public function getIsOnline(): bool {
		return $this->isOnline;
	}

	/**
	 * @param bool $isOnline
	 */
	public function setIsOnline(bool $isOnline): void {
		$this->isOnline = $isOnline;
	}

	/**
	 * @return bool
	 */
	public function getStartable(): bool {
		return $this->startable;
	}

	/**
	 * @param bool $startable
	 */
	public function setStartable(bool $startable): void {
		$this->startable = $startable;
	}

	/**
	 * @return bool
	 */
	public function getStoppable(): bool {
		return $this->stoppable;
	}

	/**
	 * @param bool $stoppable
	 */
	public function setStoppable(bool $stoppable): void {
		$this->stoppable = $stoppable;
	}

	/**
	 * @return string
	 */
	public function getIdentifier(): string {
		return $this->identifier;
	}

	/**
	 * @param string $identifier
	 */
	public function setIdentifier(string $identifier): void {
		$this->identifier = $identifier;
	}

	/**
	 * @return string
	 */
	public function getName(): string {
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName(string $name): void {
		$this->name = $name;
	}

	/**
	 * @return bool
	 */
	public function getUpdatable(): bool {
		return $this->updatable;
	}

	/**
	 * @param bool $updatable
	 */
	public function setUpdatable(bool $updatable): void {
		$this->updatable = $updatable;
	}

	/**
	 * @return bool
	 */
	public function getBackupable(): bool {
		return $this->backupable;
	}

	/**
	 * @param bool $backupable
	 */
	public function setBackupable(bool $backupable): void {
		$this->backupable = $backupable;
	}
}