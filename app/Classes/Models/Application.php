<?php

namespace ServerControlPanel\Models;

use ServerControlPanel\Interfaces\ConfigureableInterface;
use ServerControlPanel\Interfaces\OnlineableInterface;
use ServerControlPanel\Models\Configuration\ConfigFile;
use ServerControlPanel\Traits\Handleable;
use ServerControlPanel\Traits\OnlineableTrait;
use ServerControlPanel\Traits\StartStoppable;
use ServerControlPanel\Traits\StartStoppableTrait;

class Application extends HandleableEntity {

	/**
	 * @var string
	 */
	protected $name;
	/**
	 * @var string
	 */
	protected $type;
	/**
	 * @var string
	 */
	protected $dir;
	/**
	 * @var Server
	 */
	protected $server;
	/**
	 * @var integer
	 */
	protected $port;
	/**
	 * @var string
	 */
	protected $additionalInfos;
	/**
	 * @var bool
	 */
	protected $configurable = false;
	/**
	 * @var int
	 */
	protected $startSleepTime = 15;
	/**
	 * @var int
	 */
	protected $stopSleepTime = 10;
	/**
	 * @var ConfigFile[]
	 */
	protected $configFiles;

	/**
	 * @param ConfigFile[] $configFiles
	 */
	public function setConfigFiles(array $configFiles): void {
		$this->configFiles = $configFiles;
	}

	public function getConfigFiles() {
		return $this->configFiles;
	}

	/**
	 * @return string
	 */
	public function getName(): string {
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName(string $name): void {
		$this->name = $name;
	}

	/**
	 * @return string
	 */
	public function getDir(): string {
		return $this->dir;
	}

	/**
	 * @param string $dir
	 */
	public function setDir(string $dir): void {
		$this->dir = $dir;
	}

	/**
	 * @return Server
	 */
	public function getServer(): Server {
		return $this->server;
	}

	/**
	 * @param Server $server
	 */
	public function setServer(Server $server): void {
		$this->server = $server;
	}

	/**
	 * @return int
	 */
	public function getPort(): int {
		return $this->port;
	}

	/**
	 * @param int $port
	 */
	public function setPort(int $port): void {
		$this->port = $port;
	}

	/**
	 * @return string
	 */
	public function getAdditionalInfos(): ?string {
		return $this->additionalInfos;
	}

	/**
	 * @param string $additionalInfos
	 */
	public function setAdditionalInfos(string $additionalInfos): void {
		$this->additionalInfos = $additionalInfos;
	}

	/**
	 * @return bool
	 */
	public function getConfigurable(): bool {
		return $this->configurable;
	}

	/**
	 * @param bool $configurable
	 */
	public function setConfigurable(bool $configurable): void {
		$this->configurable = $configurable;
	}

	/**
	 * @return int
	 */
	public function getStartSleepTime(): int {
		return $this->startSleepTime;
	}

	/**
	 * @param int $startSleepTime
	 */
	public function setStartSleepTime(int $startSleepTime): void {
		$this->startSleepTime = $startSleepTime;
	}

	/**
	 * @return int
	 */
	public function getStopSleepTime(): int {
		return $this->stopSleepTime;
	}

	/**
	 * @param int $stopSleepTime
	 */
	public function setStopSleepTime(int $stopSleepTime): void {
		$this->stopSleepTime = $stopSleepTime;
	}

	/**
	 * @return string
	 */
	public function getType(): string {
		return $this->type;
	}

	/**
	 * @param string $type
	 */
	public function setType(string $type): void {
		$this->type = $type;
	}
}