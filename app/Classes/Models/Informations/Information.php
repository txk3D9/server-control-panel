<?php

namespace ServerControlPanel\Models\Informations;

use ServerControlPanel\Interfaces\OnlineableInterface;
use ServerControlPanel\Interfaces\StartStoppableInterface;
use ServerControlPanel\Models\HandleableEntity;
use ServerControlPanel\Models\Server;
use ServerControlPanel\Traits\OnlineableTrait;
use ServerControlPanel\Traits\StartStoppableTrait;

abstract class Information {

	/**
	 * @var string
	 */
	protected $name;
	/**
	 * @var string
	 */
	protected $text;

	/**
	 * @return string
	 */
	public function getName(): string {
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName(string $name): void {
		$this->name = $name;
	}

	/**
	 * @return string
	 */
	public function getText(): ?string {
		return $this->text;
	}

	/**
	 * @param string $text
	 */
	public function setText(string $text = null): void {
		$this->text = $text;
	}
}