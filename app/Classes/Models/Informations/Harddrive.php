<?php

namespace ServerControlPanel\Models\Informations;

class Harddrive extends Information {

	/**
	 * @var string
	 */
	protected $rootDir;

	/**
	 * @return string
	 */
	public function getRootDir(): string {
		return $this->rootDir;
	}

	/**
	 * @param string $rootDir
	 */
	public function setRootDir(string $rootDir): void {
		$this->rootDir = $rootDir;
	}
}