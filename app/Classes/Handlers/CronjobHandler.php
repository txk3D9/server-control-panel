<?php

namespace ServerControlPanel\Handlers;

use ServerControlPanel\Cronjobs\AbstractCronjob;

class CronjobHandler {

	public function handleRequest($name) {
		$escapedName = preg_replace('/[\W]/', '', $name);
		$cronjobClassName = '\\ServerControlPanel\\Cronjobs\\'.ucfirst($escapedName).'Cronjob';
		if (!class_exists($cronjobClassName)) {
			throw new \Exception('no cronjob found');
		}
		/**
		 * @var AbstractCronjob $cronjob
		 */
		$cronjob = new $cronjobClassName();
		$cronjob->execute();
	}
}