<?php

namespace ServerControlPanel\Handlers;

use ServerControlPanel\Controllers\HandleableController;
use ServerControlPanel\Controllers\ConfigureController;
use ServerControlPanel\Utilities\StatusUtility;
use ServerControlPanel\Utilities\ActionService;
use ServerControlPanel\Utilities\CacheUtility;
use ServerControlPanel\Utilities\ConfigUtility;
use ServerControlPanel\Utilities\HandleableEntityFinderUtility;
use ServerControlPanel\Utilities\TemplateUtility;

class RequestHandler {

	public function handleRequest() {
		StatusUtility::setServerAndApplicationStatus();
		$action = array_key_exists('action', $_GET) ? $_GET['action'] : null;
		try {
			if ($action) {
				$controller = null;
				if (in_array($action, HandleableController::WHITELISTED_ACTIONS)) {
					$controller = new HandleableController();
				} else if ($action == 'configure') {
					$controller = new ConfigureController();
				} else if ($action == 'resetShutdownTimer') {
					$server = HandleableEntityFinderUtility::findHandleableEntity($_GET['server']);
					if ($server) {
						StatusUtility::setStatus($server, StatusUtility::PARAM_NEXT_SHUTDOWN_AT, time() + $server->getAutoShutdownMaxUpTime());
					}
				} else if ($action == 'refreshStatus') {
					StatusUtility::rebuildCache();
				} else {
					throw new \Exception('Invalid action "'.$action.'"');
				}
				if ($controller) {
					$controller->handleByRequest($_REQUEST);
				}
				$this->redirectToHome();
			}
		} catch (\Exception | \Throwable $e) {
			echo $e->getMessage();
		} finally {
			$this->showOverview();
		}
	}

	private function showOverview() {
		echo TemplateUtility::getHtml('Overview', [
			'servers' => (ConfigUtility::loadConfig())->getServers()
		]);
	}

	private function redirectToHome() {
		header("Location: /");
		exit();
	}
}