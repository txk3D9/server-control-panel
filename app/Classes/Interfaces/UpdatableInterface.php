<?php

namespace ServerControlPanel\Interfaces;

interface UpdatableInterface {

	function update();
}