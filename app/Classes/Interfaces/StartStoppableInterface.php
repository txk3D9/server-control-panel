<?php

namespace ServerControlPanel\Interfaces;

interface StartStoppableInterface {

	function start();
	function stop();
}