<?php

namespace ServerControlPanel\Interfaces;

interface BackupableInterface {

	function backup();
}