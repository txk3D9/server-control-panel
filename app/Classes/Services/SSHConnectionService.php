<?php

namespace ServerControlPanel\Services;

use ServerControlPanel\Exceptions\SSHCommandException;
use ServerControlPanel\Models\Application;
use ServerControlPanel\Models\HandleableEntity;
use ServerControlPanel\Models\Server;

class SSHConnectionService {

	private $connection;
	/**
	 * @var Server
	 */
	private $currentlyConnectedTo;

	/**
	 * @param $command
	 * @return false|string
	 * @throws SSHCommandException
	 */
	public function execute($command) {
		if (!$this->connection) {
			throw new \Exception('currently not connected to a server');
		}
		$stream = ssh2_exec($this->connection, $command);
		$errorStream = ssh2_fetch_stream($stream, SSH2_STREAM_STDERR);
		stream_set_blocking($errorStream, true);
		stream_set_blocking($stream, true);
		$output = stream_get_contents($stream);
		$error = stream_get_contents($errorStream);
		fclose($errorStream);
		fclose($stream);
		if ($error || $output === false) {
			throw new SSHCommandException($error);
		}

		return $output;
	}

	public function connectToServerByHandleableEntity(HandleableEntity $handleableEntity) {
		if ($handleableEntity instanceof Server) {
			$this->connectToServerIfNotConnected($handleableEntity);
		} else if ($handleableEntity instanceof Application) {
			$this->connectToServerIfNotConnected($handleableEntity->getServer());
		} else {
			throw new \Exception('cant connect to server by handleable entity');
		}
	}

	public function connectToServerIfNotConnected(Server $server) {
		if ($this->currentlyConnectedTo != $server) {
			$this->connect($server);
		}
	}

	public function readFile($filePath) {
		return $this->execute("cat ".$filePath);
	}

	public function writeFile($filePath, $content) {
		$tempFilePath = "/tmp/".uniqid("temp_").".txt";
		file_put_contents($tempFilePath, $content);
		$return = ssh2_scp_send($this->connection, $tempFilePath, $filePath);
		unlink($tempFilePath);

		return $return;
	}

	protected function connect(Server $server) {
		$this->connection = ssh2_connect($server->getLocalIp(), 22);
		ssh2_auth_password($this->connection, $server->getUsername(), $server->getPassword());
		$this->currentlyConnectedTo = $server;
	}
}