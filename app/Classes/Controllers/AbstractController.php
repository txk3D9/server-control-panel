<?php

namespace ServerControlPanel\Controllers;

use ServerControlPanel\Interfaces\ConfigureableInterface;
use ServerControlPanel\Services\SSHConnectionService;

abstract class AbstractController {

	/**
	 * @var SSHConnectionService $sshConnectionService
	 */
	protected $sshConnectionService;

	public function __construct() {
		$this->sshConnectionService = new SSHConnectionService();
	}

	/**
	 * @param $request
	 */
	public abstract function handleByRequest($request): void;
}