<?php

namespace ServerControlPanel\Controllers;

use ServerControlPanel\Interfaces\ConfigureableInterface;
use ServerControlPanel\Models\Application;
use ServerControlPanel\Models\HandleableEntity;
use ServerControlPanel\Utilities\HandleableEntityFinderUtility;
use ServerControlPanel\Utilities\TemplateUtility;

class ConfigureController extends AbstractController {

	public function handleByRequest($request) : void {
		$handleableEntity = $this->getHandleableEntityByRequest($request);
		$this->sshConnectionService->connectToServerByHandleableEntity($handleableEntity);
		$saved = false;
		if ($request['save'] || $request['save_and_restart']) {
			$this->saveByRequest($request, $handleableEntity);
//			if ($request['save_and_restart']) {
//			}
			$saved = true;
		}
		$configFiles = $handleableEntity->getConfigFiles();
		foreach ($configFiles as $configFile) {
			$configFile->setContent(
				$this->sshConnectionService->readFile($handleableEntity->getDir().$configFile->getRelativePath())
			);
		}
		echo TemplateUtility::getHtml('Configure', [
			'configFiles' => $configFiles,
			'handleableEntity' => $handleableEntity,
			'request' => $request,
			'saved' => $saved,
		]);
	}

	/**
	 * @param $request
	 * @param HandleableEntity|ConfigureableInterface $handleableEntity
	 */
	private function saveByRequest($request, HandleableEntity $handleableEntity) {
		$configFiles = $handleableEntity->getConfigFiles();
		foreach ($configFiles as $key => $configFile) {
			if (array_key_exists('file_'.$key, $request)) {
				if (!$configFile->getReadOnly()) {
					$this->sshConnectionService->writeFile($handleableEntity->getDir().$configFile->getRelativePath(), $request['file_'.$key]);
				}
			}
		}
	}

	/**
	 * @param $request
	 * @return ConfigureableInterface|Application
	 * @throws \Exception
	 */
	private function getHandleableEntityByRequest($request) {
		$serverIdentifier = $request['server'];
		$applicationIdentifier = $request['application'];
		if (!$serverIdentifier || !$applicationIdentifier) {
			throw new \Exception('no server or application');
		}
		$application = HandleableEntityFinderUtility::findHandleableEntity($serverIdentifier, $applicationIdentifier);
		if (!$application || !$application instanceof Application) {
			throw new \Exception('application not found');
		}
		if (!$application->getConfigurable()) {
			throw new \Exception('application is not configurable');
		}

		return $application;
	}
}