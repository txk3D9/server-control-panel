<?php

namespace ServerControlPanel\Controllers;

use ServerControlPanel\Interfaces\ConfigureableInterface;
use ServerControlPanel\Utilities\CommanderUtility;
use ServerControlPanel\Utilities\HandleableEntityFinderUtility;

class HandleableController extends AbstractController {

	const WHITELISTED_ACTIONS = ['start', 'stop', 'update', 'backup'];

	/**
	 * @param $request
	 * @throws \Exception
	 */
	public function handleByRequest($request): void {
		$handleableEntity = HandleableEntityFinderUtility::findHandleableEntity($request['server'], $request['application']);
		$commander = CommanderUtility::getByHandleableEntity($handleableEntity);
		if ($request['action'] == 'start' && $handleableEntity->getStartable() && !$handleableEntity->getIsOnline()) {
			$commander->start();
		} else if ($request['action'] == 'stop' && $handleableEntity->getStoppable() && $handleableEntity->getIsOnline()) {
			$commander->stop();
		} else if ($request['action'] == 'update' && $handleableEntity->getUpdatable()) {
			$commander->update();
		} else if ($request['action'] == 'backup' && $handleableEntity->getBackupable()) {
			$commander->backup();
		}
	}
}