<?php

namespace ServerControlPanel\Utilities;

use ServerControlPanel\Exceptions\CacheFileInvalidException;

class CacheUtility {

	public static function set($key, $value) {
		$success = file_put_contents(CacheUtility::getCacheDir().$key, $value);
		if($success === false) {
			throw new \Exception('cache could not be written! key: '.$key);
		}
	}

	public static function get($key) {
		$cacheFile = CacheUtility::getCacheDir().$key;
		if (!file_exists($cacheFile)) {
			throw new CacheFileInvalidException();
		}

		return file_get_contents($cacheFile);
	}

	public static function remove($key) {
		$cacheFile = CacheUtility::getCacheDir().$key;
		if (file_exists($cacheFile)) {
			unlink($cacheFile);
		}
	}

	private static function getCacheDir() {
		return dirname(__FILE__).'/../../cache/';
	}
}
