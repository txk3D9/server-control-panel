<?php

namespace ServerControlPanel\Utilities;

use ServerControlPanel\Models\Application;
use ServerControlPanel\Models\Config;
use ServerControlPanel\Models\Server;

class HandleableEntityFinderUtility {

	/**
	 * @param string $serverIdentifier
	 * @param string|null $applicationIdentifier
	 * @return Application|Server
	 */
	public static function findHandleableEntity(string $serverIdentifier, string $applicationIdentifier = null) {
		$config = ConfigUtility::loadConfig();
		foreach ($config->getServers() as $server) {
			if ($server->getIdentifier() == $serverIdentifier) {
				if ($applicationIdentifier) {
					foreach ($server->getApplications() as $application) {
						if ($application->getIdentifier() == $applicationIdentifier) {
							return $application;
						}
					}
				} else {
					return $server;
				}
			}
		}
	}
}