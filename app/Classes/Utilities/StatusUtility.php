<?php

namespace ServerControlPanel\Utilities;

use ServerControlPanel\Exceptions\CacheFileInvalidException;
use ServerControlPanel\Exceptions\SSHCommandException;
use ServerControlPanel\Models\Application;
use ServerControlPanel\Models\HandleableEntity;
use ServerControlPanel\Models\Server;
use ServerControlPanel\Services\SSHConnectionService;

class StatusUtility {

	const CACHE_FILE_NAME = 'status';
	const STATUS_ONLINE = 'online';
	const STATUS_OFFLINE = 'offline';
	const STATUS_UPDATING = 'updating';
	const PARAM_STATUS = 'status';
	const PARAM_NEXT_SHUTDOWN_AT = 'nextShutdownAt';

	private static $cache = null;

	/**
	 * @throws \Exception
	 */
	public static function setServerAndApplicationStatus() {
		//@todo remove this method and use the StatusUtility::getStatus() method directly where needed. also remove the isOnline for HandleableEntity.
		$servers = ConfigUtility::loadConfig()->getServers();
		$cache = self::getCache();
		if (count($servers) > 0) {
			foreach ($servers as $server) {
				$serverIdentifier =  self::getIdentifierByHandleableEntity($server);
				$server->setIsOnline($cache[$serverIdentifier][self::PARAM_STATUS] == self::STATUS_ONLINE);
				if ($server->getAutoShutdownEnabled()) {
					$server->setNextShutdownAt((int) $cache[$serverIdentifier][self::PARAM_NEXT_SHUTDOWN_AT]);
				}
				if($server->getIsOnline()) {
					foreach ($server->getApplications() as $application) {
						$applicationIdentifier = self::getIdentifierByHandleableEntity($application);
						$application->setIsOnline($cache[$applicationIdentifier][self::PARAM_STATUS] == self::STATUS_ONLINE);
					}
				}
			}
		}
	}

	public static function rebuildCache($useCurrentCacheAsBase = true) {
		$servers = ConfigUtility::loadConfig()->getServers();
		$cache = ($useCurrentCacheAsBase) ? self::getCache() : [];
		foreach ($servers as $server) {
			$connectionService = new SSHConnectionService();
			$serverCommander = CommanderUtility::getByHandleableEntity($server, $connectionService);
			try {
				$serverIdentifier = self::getIdentifierByHandleableEntity($server);
				$cache[$serverIdentifier][self::PARAM_STATUS] = ($serverCommander->isOnline()) ? self::STATUS_ONLINE : self::STATUS_OFFLINE;
				if ($cache[$serverIdentifier][self::PARAM_STATUS] == self::STATUS_ONLINE && count($server->getApplications()) > 0) {
					foreach ($server->getApplications() as $application) {
						$applicationCommander = CommanderUtility::getByHandleableEntity($application, $connectionService);
						$applicationIdentifier = self::getIdentifierByHandleableEntity($application);
						$cache[$applicationIdentifier][self::PARAM_STATUS] = ($applicationCommander->isOnline()) ? self::STATUS_ONLINE : self::STATUS_OFFLINE;
					}
				}
			} catch (SSHCommandException $e) {
				echo $e->getMessage();
			}
		}
		CacheUtility::set(self::CACHE_FILE_NAME, json_encode($cache));
	}

	public static function setStatus(HandleableEntity $handleableEntity, $parameter, $value) {
		$cache = self::getCache();
		$identifier = self::getIdentifierByHandleableEntity($handleableEntity);
		$cache[$identifier][$parameter] = $value;
		CacheUtility::set(self::CACHE_FILE_NAME, json_encode($cache));
		self::$cache = $cache;
	}

	public static function getStatus(HandleableEntity $handleableEntity, $parameter) {
		$cache = self::getCache();
		$identifier = self::getIdentifierByHandleableEntity($handleableEntity);

		return $cache[$identifier][$parameter];
	}

	private static function getCache() {
		try {
			if(self::$cache === null) {
				self::$cache = json_decode(CacheUtility::get(self::CACHE_FILE_NAME), true);
			}
			return self::$cache;
		} catch (CacheFileInvalidException $e) {
			StatusUtility::rebuildCache(false);

			return self::getCache();
		}
	}

	private static function getIdentifierByHandleableEntity(HandleableEntity $handleableEntity) {
		if ($handleableEntity instanceof Server) {
			return $handleableEntity->getIdentifier();
		} else if ($handleableEntity instanceof Application) {
			return $handleableEntity->getServer()->getIdentifier().'_'.$handleableEntity->getIdentifier();
		} else {
			throw new \Exception('cant get identifier for '.get_class($handleableEntity));
		}
	}
}