<?php

namespace ServerControlPanel\Utilities;

use ServerControlPanel\Models\Application;
use ServerControlPanel\Models\Config;
use ServerControlPanel\Models\Server;

class TemplateUtility {

	public static function getHtml(string $template, array $arguments = []): string {
		$loader = new \Twig\Loader\FilesystemLoader([
			dirname(__FILE__).'/../../Resources/Private/Layouts/',
			dirname(__FILE__).'/../../Resources/Private/Partials/',
			dirname(__FILE__).'/../../Resources/Private/Templates/'
		]);
		$twig = new \Twig\Environment($loader, [
//			'cache' => dirname(__FILE__).'/../../cache',
		]);

		return $twig->render($template.'.html.twig', $arguments);
	}
}