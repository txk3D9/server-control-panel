<?php

namespace ServerControlPanel\Utilities;

use ServerControlPanel\Models\Application;
use ServerControlPanel\Models\Configuration\ConfigFile;
use ServerControlPanel\Models\Informations\Harddrive;
use ServerControlPanel\Models\Informations\Information;
use ServerControlPanel\Models\Nextcloud;
use ServerControlPanel\Models\Config;
use ServerControlPanel\Models\Server;

class ConfigUtility {

	const CONFIG_FILE = 'config.json';
	private static $config = null;

	public static function loadConfig(): Config {
		if (ConfigUtility::$config === null) {
			$data = json_decode(file_get_contents(dirname(__FILE__).'/../../'.ConfigUtility::CONFIG_FILE), true);
			if (!$data) {
				throw new \Exception('No valid config file');
			}
			$config = new Config();
			$servers = [];
			if (array_key_exists('servers', $data)) {
				foreach ($data['servers'] as $serverData) {
					$server = new Server();
					foreach ($serverData as $key => $data) {
						if ($key == 'applications') {
							ConfigUtility::setApplications($server, $data);
						} else if ($key == 'informations') {
							ConfigUtility::setInformations($server, $data);
						} else if ($key == 'disabled' && $data == true) {
							continue 2;
						} else {
							$setFunction = 'set'.ucfirst($key);
							if (method_exists($server, $setFunction)) {
								$server->$setFunction($data);
							}
						}
					}
					$servers[] = $server;
				}
				ksort($servers);
				$config->setServers($servers);
			}
			ConfigUtility::$config = $config;
		}

		return ConfigUtility::$config;
	}

	private static function setApplications(Server $server, $applicationsData) {
		$applications = [];
		foreach ($applicationsData as $applicationData) {
			if (array_key_exists('disabled', $applicationData) && $applicationData['disabled'] == true) {
				continue;
			}
			$application = new Application();
			foreach ($applicationData as $key => $value) {
				if($key == 'configFiles') {
					ConfigUtility::setConfigFiles($application, $value);
				} else {
					$setFunction = 'set'.ucfirst($key);
					if (method_exists($application, $setFunction)) {
						$application->$setFunction($value);
					}
				}

			}
			$application->setServer($server);
			$applications[$application->getName()] = $application;
		}
		ksort($applications);
		$server->setApplications($applications);
	}

	private static function setInformations(Server $server, $informationsData) {
		$informations = [];
		foreach ($informationsData as $key => $informationData) {
			$className = '\\ServerControlPanel\\Models\\'.ucfirst($informationData['type']);
			if (class_exists($className)) {
				$information = new $className();
				if ($information instanceof Information) {
					$information->setName($informationData['name']);
					$information->setText($informationData['text']);
					if ($information instanceof Harddrive) {
						$information->setRootDir($informationData['rootDir']);
					}
					$informations[$information->getName()] = $information;
				}
			}
		}
		ksort($informations);
		$server->setInformations($informations);
	}

	private static function setConfigFiles(Application $application, array $configFilesData) {
		$configFiles = [];
		foreach ($configFilesData as $configFileData) {
			$readOnly = array_key_exists('readOnly', $configFileData) ? (bool) $configFileData['readOnly'] : false;
			$configFiles[] = new ConfigFile($configFileData['file'], $readOnly);
		}
		$application->setConfigFiles($configFiles);
	}
}