<?php

namespace ServerControlPanel\Utilities;


use ServerControlPanel\Commanders\AbstractCommander;
use ServerControlPanel\Commanders\ApplicationCommander;
use ServerControlPanel\Commanders\ServerCommander;
use ServerControlPanel\Models\Application;
use ServerControlPanel\Models\HandleableEntity;
use ServerControlPanel\Models\Server;
use ServerControlPanel\Services\SSHConnectionService;

class CommanderUtility {

	public static function getByHandleableEntity(HandleableEntity $handleableEntity, $connectionService = null): AbstractCommander {
		if($handleableEntity instanceof Server) {
			$commanderClassName = ServerCommander::class;
		} else if ($handleableEntity instanceof Application) {
			$commanderClassName = '\\ServerControlPanel\\Commanders\\'.ucfirst($handleableEntity->getType()).'Commander';
			if(!class_exists($commanderClassName)) {
				$commanderClassName = ApplicationCommander::class;
			}
		}
		if(!$connectionService) {
			$connectionService = new SSHConnectionService();
		}
		/**
		 * @var AbstractCommander $commander
		 */
		return new $commanderClassName($connectionService, $handleableEntity);
	}
}