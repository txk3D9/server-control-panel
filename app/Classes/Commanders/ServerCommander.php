<?php

namespace ServerControlPanel\Commanders;

use ServerControlPanel\Exceptions\SSHCommandException;
use ServerControlPanel\Models\Server;
use ServerControlPanel\Services\SSHConnectionService;
use ServerControlPanel\Utilities\CacheUtility;
use ServerControlPanel\Utilities\StatusUtility;

class ServerCommander extends AbstractCommander {

	/**
	 * ServerCommander constructor.
	 * @param SSHConnectionService $connectionService
	 * @param Server $handleableEntity
	 * @throws \Exception
	 */
	public function __construct(SSHConnectionService $connectionService, Server $handleableEntity) {
		parent::__construct($connectionService, $handleableEntity);
	}

	/**
	 * @return mixed
	 * @throws SSHCommandException
	 */
	public function isOnline(): bool {
		exec('ping -c 1 '.$this->handleableEntity->getLocalIp(), $output);
		if ($output === false) {
			throw new SSHCommandException();
		}

		return strpos($output[1], 'Destination Host Unreachable') === false && strpos($output[3], '100% packet loss') === false;
	}

	/**
	 * @return mixed
	 * @throws SSHCommandException
	 */
	public function start(): void {
		exec('wakeonlan '.str_replace('-', ':', $this->handleableEntity->getMacAddress()), $output, $error);
		if($this->handleableEntity->getAutoShutdownEnabled()) {
			StatusUtility::setStatus($this->handleableEntity, StatusUtility::PARAM_NEXT_SHUTDOWN_AT, time() + $this->handleableEntity->getAutoShutdownMaxUpTime());
		}
		StatusUtility::setStatus($this->handleableEntity, StatusUtility::PARAM_STATUS, StatusUtility::STATUS_ONLINE);
	}

	public function stop(): void { //supsends the server
		$this->connectionService->connectToServerIfNotConnected($this->handleableEntity);
		try {
			StatusUtility::setStatus($this->handleableEntity, StatusUtility::PARAM_STATUS, StatusUtility::STATUS_OFFLINE);
			$this->connectionService->execute('echo '.$this->handleableEntity->getPassword().' | sudo -S systemctl suspend');
		} catch (\Exception $e) {
			if (strpos(trim($e->getMessage()) != '[sudo] password for '.$this->handleableEntity->getUsername().':')) {
				throw $e;
			}
		}
	}
}