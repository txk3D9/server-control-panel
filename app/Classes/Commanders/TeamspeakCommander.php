<?php

namespace ServerControlPanel\Commanders;

use ServerControlPanel\Exceptions\SSHCommandException;
use ServerControlPanel\Models\Application;
use ServerControlPanel\Models\Minecraft;

class TeamspeakCommander extends ApplicationCommander {

	public function isOnline(): bool {
		$this->connectionService->connectToServerIfNotConnected($this->handleableEntity->getServer());
		$ts3ProcessPid = trim($this->connectionService->execute('pgrep ts3server'));

		return (bool) $ts3ProcessPid;
	}
}