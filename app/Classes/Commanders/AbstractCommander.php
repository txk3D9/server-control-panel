<?php

namespace ServerControlPanel\Commanders;

use ServerControlPanel\Interfaces\StartStoppableInterface;
use ServerControlPanel\Models\HandleableEntity;
use ServerControlPanel\Services\SSHConnectionService;

abstract class AbstractCommander implements StartStoppableInterface {

	/**
	 * @var SSHConnectionService
	 */
	protected $connectionService;
	/**
	 * @var HandleableEntity
	 */
	protected $handleableEntity;

	public function __construct(SSHConnectionService $connectionService, HandleableEntity $handleableEntity) {
		$this->connectionService = $connectionService;
		$this->handleableEntity = $handleableEntity;
	}

	abstract function isOnline(): bool;
}