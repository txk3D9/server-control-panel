<?php

namespace ServerControlPanel\Commanders;

use ServerControlPanel\Interfaces\BackupableInterface;
use ServerControlPanel\Interfaces\UpdatableInterface;
use ServerControlPanel\Models\Application;
use ServerControlPanel\Services\SSHConnectionService;
use ServerControlPanel\Utilities\StatusUtility;

class ApplicationCommander extends AbstractCommander implements UpdatableInterface, BackupableInterface {

	const LOG_FILE_NAME = 'screen_log.log';
	protected $screenLogEnabled = true;

	public function __construct(SSHConnectionService $connectionService, Application $application) {
		parent::__construct($connectionService, $application);
		$this->connectionService->connectToServerIfNotConnected($application->getServer());
	}

	public function isOnline(): bool {
		$this->connectionService->connectToServerIfNotConnected($this->handleableEntity->getServer());
		$screenExists = trim($this->connectionService->execute(
				'screen -S '.$this->handleableEntity->getIdentifier().' -X select . ; echo $?'
			)) === "0";

		return $screenExists;
	}

	public function start(): void {
		$identifier = $this->handleableEntity->getIdentifier();
		if ($this->isOnline()) {
			throw new \Exception($identifier.' is already running');
		} else {
			#start screen
			$startScreenCmd = 'screen -Sdm '.$identifier;
			if ($this->screenLogEnabled) {
				$startScreenCmd .= ' -L -Logfile '.$this->handleableEntity->getDir().ApplicationCommander::LOG_FILE_NAME;
			}
			$this->connectionService->execute(
				$startScreenCmd
			);
			#start server
			$this->connectionService->execute(
				'screen -S '.$identifier.' -X stuff "cd '.$this->handleableEntity->getDir().' && ./start.sh^M"'
			);
			sleep($this->handleableEntity->getStartSleepTime());
			StatusUtility::setStatus($this->handleableEntity, StatusUtility::PARAM_STATUS, StatusUtility::STATUS_ONLINE);
		}
	}

	public function stop(): void {
		if ($this->isOnline()) {
			#stop server
			$this->connectionService->execute('screen -S '.$this->handleableEntity->getIdentifier().' -X stuff "^C"');
			sleep($this->handleableEntity->getStopSleepTime());
			#kill screen
			$this->connectionService->execute('screen -S '.$this->handleableEntity->getIdentifier().' -X kill');
			StatusUtility::setStatus($this->handleableEntity, StatusUtility::PARAM_STATUS, StatusUtility::STATUS_OFFLINE);
		} else {
			throw new \Exception($this->handleableEntity->getIdentifier().' is not running');
		}
	}

	function update() {
		if (!$this->handleableEntity->getUpdatable()) {
			throw new \Exception('not updatable');
		}
		StatusUtility::setStatus($this->handleableEntity, StatusUtility::PARAM_STATUS, StatusUtility::STATUS_UPDATING);
		$wasOnline = false;
		if ($this->isOnline()) {
			$wasOnline = true;
			$this->stop();
		}
		#update server
		$this->connectionService->execute(
			'cd '.$this->handleableEntity->getDir().' && ./update.sh'
		);
		if ($wasOnline) {
			$this->start();
		} else {
			StatusUtility::setStatus($this->handleableEntity, StatusUtility::PARAM_STATUS, StatusUtility::STATUS_OFFLINE);
		}
	}

	function backup() {
		if (!$this->handleableEntity->getBackupable()) {
			throw new \Exception('not backupable');
		}
		$this->connectionService->execute(
			'cd '.$this->handleableEntity->getDir().' && ./backup.sh'
		);
	}
}