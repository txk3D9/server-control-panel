<?php

namespace ServerControlPanel\Cronjobs;


use ServerControlPanel\Utilities\ActionService;

abstract class AbstractCronjob {

	abstract function execute();
}