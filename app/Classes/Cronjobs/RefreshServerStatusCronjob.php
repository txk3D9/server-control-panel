<?php

namespace ServerControlPanel\Cronjobs;

use ServerControlPanel\Commanders\ServerCommander;
use ServerControlPanel\Exceptions\CacheFileInvalidException;
use ServerControlPanel\Utilities\ActionService;
use ServerControlPanel\Utilities\CacheUtility;
use ServerControlPanel\Utilities\CommanderUtility;
use ServerControlPanel\Utilities\ConfigUtility;
use ServerControlPanel\Utilities\StatusUtility;

class RefreshServerStatusCronjob extends AbstractCronjob {

	function execute() {
		StatusUtility::rebuildCache();
	}
}