<?php

namespace ServerControlPanel\Cronjobs;

use ServerControlPanel\Commanders\ServerCommander;
use ServerControlPanel\Exceptions\CacheFileInvalidException;
use ServerControlPanel\Utilities\ActionService;
use ServerControlPanel\Utilities\CacheUtility;
use ServerControlPanel\Utilities\CommanderUtility;
use ServerControlPanel\Utilities\ConfigUtility;
use ServerControlPanel\Utilities\StatusUtility;

class ShutdownOverdueServersCronjob extends AbstractCronjob {

	function execute() {
		$config = ConfigUtility::loadConfig();
		foreach ($config->getServers() as $server) {
			if ($server->getAutoShutdownEnabled()) {
				if (StatusUtility::getStatus($server, StatusUtility::PARAM_STATUS) == StatusUtility::STATUS_ONLINE) {
					try {
						$shutdownAt = (int) StatusUtility::getStatus($server, StatusUtility::PARAM_NEXT_SHUTDOWN_AT);
						if ($shutdownAt < time()) {
							/**
							 * @var ServerCommander $commander
							 */
							$commander = CommanderUtility::getByHandleableEntity($server);
							$commander->stop();
							StatusUtility::setStatus($server, StatusUtility::PARAM_STATUS, StatusUtility::STATUS_OFFLINE);
						}
					} catch (CacheFileInvalidException $e) {
					}
				}
			}
		}
	}
}