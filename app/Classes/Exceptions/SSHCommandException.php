<?php

namespace ServerControlPanel\Exceptions;

class SSHCommandException extends \Exception {

	protected $code = 1597339594;
	protected $message = 'Error while executing an SSH command';
}