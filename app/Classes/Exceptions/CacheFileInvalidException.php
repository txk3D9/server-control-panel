<?php

namespace ServerControlPanel\Exceptions;

class CacheFileInvalidException extends \Exception {

	protected $code = 1597339510;
	protected $message = 'Cache file is not available';
}