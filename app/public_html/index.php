<?php
ini_set('display_errors', 1);
error_reporting (E_ERROR);

require_once dirname(__FILE__).'/../vendor/autoload.php';

$scp = new \ServerControlPanel\Handlers\RequestHandler();
$scp->handleRequest();